import random, time
from django.shortcuts import render, redirect
# from .forms import TicTacToeMoveForm


def play_tic_tac_toe(request):
    # Initialize the board if it doesn't exist in the session
    if 'board' not in request.session:
        request.session['board'] = {
            '1': ' ', '2': ' ', '3': ' ',
            '4': ' ', '5': ' ', '6': ' ',
            '7': ' ', '8': ' ', '9': ' ',
        }
    if 'turns' not in request.session:
        request.session['turns'] = {
            "turn": 0,
        }
    context = {
        "board": request.session['board'],
        "choice": str(id),
    }

    return render(request, 'game/play.html', context)


def reset(request):
    if request.method == "POST":
        print('reset')
        request.session['board'] = {
                    '1': ' ', '2': ' ', '3': ' ',
                    '4': ' ', '5': ' ', '6': ' ',
                    '7': ' ', '8': ' ', '9': ' ',
                }
        request.session['turns'] = {
            "turn": 0,
        }
        request.session.modified = True
        request.session.save()
        return redirect('play_tic_tac_toe')


def player_choice(request, id):
    comp_choice = False
    game_over = " "
    choice = str(id)
    print(request.session['turns']["turn"])

    if request.session['turns']["turn"] >= 8 and game_over == " ":
        game_over = "Tie"
        print("tie")
        comp_choice = False
    if request.method == "POST" and request.session['board'][choice] == " ":
        request.session['board'][choice] = 'X'
        request.session['turns']["turn"] += 1
        request.session.modified = True
        request.session.save()

        result = check_winner(request.session['board'], comp_choice)
        if result == "player":
            game_over = "X"
            print("You win!")
            player_win()
        if game_over == "O":
            comp_choice = False
        else:
            comp_choice = True

        board = request.session['board']
        position = check_winner(board, comp_choice)

        if position != " " and game_over == " ":
            request.session['board'][position] = 'O'
            request.session['turns']["turn"] += 1
            result = check_winner(request.session['board'], comp_choice)
            if result == "computer":
                game_over = "O"
                computer_win()
            comp_choice = False

        else:
            print("beeep")

            while comp_choice and game_over == " ":
                random_choice = str(random.randrange(1,9))

                if request.session['board'][random_choice] == " ":
                    request.session['board'][random_choice] = 'O'
                    request.session['turns']["turn"] += 1

                    if check_winner(request.session['board'], comp_choice) == "computer":
                        game_over = "O"
                    comp_choice = False

            print(request.session['board'])
        request.session.modified = True
        request.session.save()
        if game_over != " ":
            url= f"/game/play/?game_over={game_over}"
            return redirect(url)

    return redirect('play_tic_tac_toe')



def player_win():
    print("You win!")


def computer_win():
    print("You lost!")



def check_winner(board, comp_choice):
    winning_combinations = [
        ['1', '2', '3'],
        ['4', '5', '6'],
        ['7', '8', '9'],
        ['1', '4', '7'],
        ['2', '5', '8'],
        ['3', '6', '9'],
        ['1', '5', '9'],
        ['3', '5', '7'],
    ]

    for combo in winning_combinations:
        a, b, c = combo
        if board[a] == "X" and board[b] == "X" and board[c] == "X":
            return "player"
        elif board[a] == "O" and board[b] == "O" and board[c] == "O":
            return "computer"
        if comp_choice:
            if board[a] == "X" and board[b] == "X" and board[c] == " ":
                return c
            elif board[a] == "X" and board[b] == " " and board[c] == "X":
                return b
            elif board[a] == " " and board[b] == "X" and board[c] == "X":
                return a
    return " "
