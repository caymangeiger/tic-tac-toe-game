from django import forms

class TicTacToeMoveForm(forms.Form):
    cell_id = forms.CharField(max_length=1)
