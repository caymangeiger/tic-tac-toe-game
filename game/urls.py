from django.urls import path
from . import views
from .views import player_choice, reset

urlpatterns = [
    path('play/', views.play_tic_tac_toe, name='play_tic_tac_toe'),
    path("choice/<str:id>", player_choice, name="choice"),
    path("reset/", reset, name="reset"),
    # Add other URL patterns for your app if needed
]
